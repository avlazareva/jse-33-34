package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@Nullable final String token) {
        this.token = token;
    }

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
