package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull final User user) {
        super(user);
    }

}
