package ru.t1.lazareva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.request.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-bin";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

}
